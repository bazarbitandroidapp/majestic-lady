package com.pureweblopment.majesticlady.Fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pureweblopment.majesticlady.Activity.MainActivity;
import com.pureweblopment.majesticlady.Adapter.TestimonialAdapter;
import com.pureweblopment.majesticlady.Global.Global;
import com.pureweblopment.majesticlady.Global.SharedPreference;
import com.pureweblopment.majesticlady.Global.StaticUtility;
import com.pureweblopment.majesticlady.Model.Testimonial;
import com.pureweblopment.majesticlady.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TestimonialFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TestimonialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TestimonialFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //Widget
    private RecyclerView mRvTestimonial;
    private ProgressBar mProgress;

    //Toolbar
    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    CardView cardviewBottomNavigation;

    public TestimonialFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TestimonialFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TestimonialFragment newInstance(String param1, String param2) {
        TestimonialFragment fragment = new TestimonialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_testimonial, container, false);

        widgetInitialization(view);
        widgetTypeface();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mProgress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        setTestimonial();
        return view;
    }

    //region widgetInitialization
    private void widgetInitialization(View view) {
        //Toolbar
        imageCartBack = getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = getActivity().findViewById(R.id.imageNavigation);
        imageLogo = getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = getActivity().findViewById(R.id.txtCatName);
        cardviewBottomNavigation = getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(getString(R.string.testimonial));

        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        //Widget
        mRvTestimonial =  view.findViewById(R.id.rvTestimonial);
        mProgress = view.findViewById(R.id.progress);
    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
    }//endregion

    private void setTestimonial(){
        ArrayList<Testimonial> testimonials = new ArrayList<>();
        testimonials.add(new Testimonial("Test", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.", "test Test"));
        testimonials.add(new Testimonial("Test", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.", "test Test"));
        testimonials.add(new Testimonial("Test", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.", "test Test"));
        TestimonialAdapter testimonialAdapter = new TestimonialAdapter(getContext(), TestimonialFragment.this, testimonials);
        mRvTestimonial.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRvTestimonial.setAdapter(testimonialAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
