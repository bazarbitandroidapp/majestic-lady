package com.pureweblopment.majesticlady.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.utils.DateUtils;
import com.pureweblopment.majesticlady.Activity.LoginActivity;
import com.pureweblopment.majesticlady.Activity.MainActivity;
import com.pureweblopment.majesticlady.Global.Global;
import com.pureweblopment.majesticlady.Global.SharedPreference;
import com.pureweblopment.majesticlady.Global.Typefaces;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;

import com.pureweblopment.majesticlady.Global.SendMail;
import com.pureweblopment.majesticlady.Global.StaticUtility;

import com.pureweblopment.majesticlady.R;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Response;

import static com.applandeo.materialcalendarview.CalendarView.MANY_DAYS_PICKER;
import static java.util.Calendar.DATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyAccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAccountFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    CardView cardviewBottomNavigation;

    private OnFragmentInteractionListener mListener;

    CircleImageView circularImageViewUser;
    /*CircularImageView circularImageViewUser;*/
    TextView txtUserName, txtUserEmail;
    ImageView imageMyAccount, imageMyAddress, imageOrderHistory, imageChangePWD, imageLogout, imageReviews;
    TextView txtMyAccount, txtMyAddress, txtOrderHistory, txtChangePWD, txtLogout,
            txtReviews, txtPeriodCycle;
    LinearLayout llMyAccount, llMyAddress, llOrderHistory, llChangePWD, llLogout, llReviews,
            llMyAccountHeader, llPeriodCycle;

    String picUrl = null;
    private static URL urla = null;
    private static URI urin = null;

    ImageView imgUser;

    private AlertDialog mPeriodDialog, mPeriodCalendarDialog;

    String userID = "";

    public static Date date = null;
    public static SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
    public static SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
    public static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public MyAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAccountFragment newInstance(String param1, String param2) {
        MyAccountFragment fragment = new MyAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.VISIBLE);
        cardviewBottomNavigation.setVisibility(View.VISIBLE);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtCatName.setText(R.string.myaccount);

        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);

        Initialization(view);
        TypeFace();
        OnClickListener();
        AppSettings();

        userID = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (userID != null) {
            txtLogout.setText(getString(R.string.logout));
            imageLogout.setImageResource(R.drawable.ic_logout);
        } else {
            txtLogout.setText(getString(R.string.login));
            imageLogout.setImageResource(R.drawable.ic_login);
        }

        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL) != null) {
            txtUserEmail.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL));
        } else {
            txtUserEmail.setText(getString(R.string.guest_email_address));
        }
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name) != null) {
            txtUserName.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name));
        } else {
            txtUserName.setText(getString(R.string.guest_user));
        }

        //region UserProfile
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture) != null) {
            String stringProfile = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture);
            if (!stringProfile.equalsIgnoreCase("")) {
                imgUser.setVisibility(View.GONE);
                circularImageViewUser.setVisibility(View.VISIBLE);
                try {
                    picUrl = String.valueOf(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture));
                    urla = new URL(picUrl);
                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    Picasso.with(getContext())
                            .load(picUrl)
                            .into(circularImageViewUser, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MyAccountFragment.java When parsing image url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            } else {
                imgUser.setVisibility(View.VISIBLE);
                circularImageViewUser.setVisibility(View.GONE);
            }
        } else {

            imgUser.setVisibility(View.VISIBLE);
            circularImageViewUser.setVisibility(View.GONE);
        }//endregion

        return view;
    }

    //region Initialization...
    private void Initialization(View view) {
        circularImageViewUser = view.findViewById(R.id.circularImageViewUser);
        imgUser = (ImageView) view.findViewById(R.id.imgUser);
        txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        txtUserEmail = (TextView) view.findViewById(R.id.txtUserEmail);

        txtMyAccount = (TextView) view.findViewById(R.id.txtMyAccount);
        txtMyAddress = (TextView) view.findViewById(R.id.txtMyAddress);
        txtOrderHistory = (TextView) view.findViewById(R.id.txtOrderHistory);
        txtChangePWD = (TextView) view.findViewById(R.id.txtChangePWD);
        txtLogout = (TextView) view.findViewById(R.id.txtLogout);
        txtReviews = (TextView) view.findViewById(R.id.txtReviews);
        txtPeriodCycle = (TextView) view.findViewById(R.id.txtPeriodCycle);

        llMyAccount = (LinearLayout) view.findViewById(R.id.llMyAccount);
        llMyAddress = (LinearLayout) view.findViewById(R.id.llMyAddress);
        llOrderHistory = (LinearLayout) view.findViewById(R.id.llOrderHistory);
        llChangePWD = (LinearLayout) view.findViewById(R.id.llChangePWD);
        llLogout = (LinearLayout) view.findViewById(R.id.llLogout);
        llReviews = (LinearLayout) view.findViewById(R.id.llReviews);
        llMyAccountHeader = (LinearLayout) view.findViewById(R.id.llMyAccountHeader);
        llPeriodCycle = (LinearLayout) view.findViewById(R.id.llPeriodCycle);

        imageMyAccount = (ImageView) view.findViewById(R.id.imageMyAccount);
        imageMyAddress = (ImageView) view.findViewById(R.id.imageMyAddress);
        imageOrderHistory = (ImageView) view.findViewById(R.id.imageOrderHistory);
        imageChangePWD = (ImageView) view.findViewById(R.id.imageChangePWD);
        imageLogout = (ImageView) view.findViewById(R.id.imageLogout);
        imageReviews = (ImageView) view.findViewById(R.id.imageReviews);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        txtUserEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtMyAccount.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtMyAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderHistory.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtChangePWD.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReviews.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPeriodCycle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region OnClickListener...
    private void OnClickListener() {
        llMyAddress.setOnClickListener(this);
        llMyAccount.setOnClickListener(this);
        llOrderHistory.setOnClickListener(this);
        llChangePWD.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llReviews.setOnClickListener(this);
        llPeriodCycle.setOnClickListener(this);
    }
    //endregion

    //region AppSettings...
    private void AppSettings() {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
            txtUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        txtMyAccount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtMyAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtOrderHistory.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtChangePWD.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtReviews.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtPeriodCycle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llMyAccountHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMyAddress:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoMyAccountAddressListing();
                } else {
                    Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llMyAccount:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoUserProfile();
                } else {
                    Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llOrderHistory:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoOrderHistory();
                } else {
                    Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llChangePWD:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoReviews();
                } else {
                    Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llReviews:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoReviews();
                } else {
                    Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llLogout:
                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    imageLogout.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                }
                if (txtLogout.getText().toString().equalsIgnoreCase(getString(R.string.logout))) {
                    llLogout.setEnabled(false);
                    Logout();
                } else {
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;

            case R.id.llPeriodCycle:
                if (mPeriodDialog != null) {
                    if (!mPeriodDialog.isShowing()) {
                        PeriodDialog();
                    }
                } else {
                    PeriodDialog();
                }
                break;
        }
    }

    //region FOR Logout API..
    private void Logout() {

        String[] key = {"user_token"};
        String[] val = {String.valueOf(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN))};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Logout);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                llLogout.setEnabled(true);
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.putExtra("Redirect", "logout");
                               /* intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
                                startActivity(intent);
                                /*getActivity().finish();*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            llLogout.setEnabled(false);
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strMessage.equalsIgnoreCase("user token mismatched!")) {
                                    Toast.makeText(getContext(), "You are already Logged in other device..!", Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MyAccountFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoReviews();

        void gotoOrderHistory();

        void gotoUserProfile();

        void gotoChangepwd();

        void gotoMyAccountAddressListing();
    }

    //region Period Dialog
    private void PeriodDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.row_dialog_period_cycle, null);

        //region Initialization
        ImageView mImgClose = view.findViewById(R.id.imgClose);
        final EditText mEdtCycle = view.findViewById(R.id.edtCycle);
        final TextView mTxtDate = view.findViewById(R.id.txtDate);
        final EditText mEdtNoOfDays = view.findViewById(R.id.edtNoOfDays);
        CardView mCvSubmit = view.findViewById(R.id.cvSubmit);
        TextView mTxtSubmit = view.findViewById(R.id.txtSubmit);
        TextView mTxtNoOfDaysNote = view.findViewById(R.id.txtNoOfDaysNote);
        TextView mTxtPeriodCycleNote = view.findViewById(R.id.txtPeriodCycleNote);
        //endregion

        //region Theme Setting
        chanageEditTextBorder(mEdtCycle);
        chanageEditTextBorder(mEdtNoOfDays);
        chanageTexViewtBorder(mTxtDate);
        mTxtNoOfDaysNote.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        mTxtPeriodCycleNote.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        mCvSubmit.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtSubmit.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        mImgClose.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        //endregion

        //region Typefaces
        mEdtCycle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mTxtDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mEdtNoOfDays.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mTxtSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mTxtNoOfDaysNote.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mTxtPeriodCycleNote.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        //endregion

        //region OnClickListener
        mImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPeriodDialog.isShowing()) {
                    mPeriodDialog.dismiss();
                }
            }
        });


        mTxtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateDialog dateDialog = new DateDialog(mTxtDate);
                dateDialog.show(getFragmentManager(), "datePicker");
            }
        });

        mCvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.hideKeyboard(getActivity());
                if (!TextUtils.isEmpty(mEdtCycle.getText().toString())) {
                    mEdtCycle.setError(null);
                    if (Integer.parseInt(mEdtCycle.getText().toString()) >= 4 &&
                            Integer.parseInt(mEdtCycle.getText().toString()) <= 7) {
                        mEdtCycle.setError(null);
                        if (!TextUtils.isEmpty(mTxtDate.getText().toString())) {
                            mTxtDate.setError(null);
                            if (!TextUtils.isEmpty(mEdtNoOfDays.getText().toString())) {
                                mEdtNoOfDays.setError(null);
                                if (Integer.parseInt(mEdtNoOfDays.getText().toString()) >= 21 &&
                                        Integer.parseInt(mEdtNoOfDays.getText().toString()) <= 30) {
                                    mEdtNoOfDays.setError(null);
                                    String strStartDate = mTxtDate.getText().toString();
                                    String strCycle = mEdtCycle.getText().toString();
                                    String strNextNoOfDays = mEdtNoOfDays.getText().toString();

                                    String strEndDate = getEndDate(strStartDate, strCycle);
                                    String strNextDate = getNextDate(strEndDate, strNextNoOfDays);
                                    mPeriodDialog.dismiss();

                                    if (mPeriodCalendarDialog != null) {
                                        if (!mPeriodCalendarDialog.isShowing()) {
                                            PeriodCalendarDialog(strStartDate, strNextDate, strCycle);
                                        }
                                    } else {
                                        PeriodCalendarDialog(strStartDate, strNextDate, strCycle);
                                    }

                                    Toast.makeText(getContext(), strNextDate, Toast.LENGTH_SHORT).show();
                                } else {
                                    mEdtNoOfDays.setError(getString(R.string.enter_valid_cycle));
                                }
                            } else {
                                mEdtNoOfDays.setError(getString(R.string.enter_how_long_your_cycle));
                            }
                        } else {
                            mTxtDate.setError(getString(R.string.select_date));
                        }
                    } else {
                        mEdtCycle.setError(getString(R.string.enter_valid_period_cycle));
                    }
                } else {
                    mEdtCycle.setError(getString(R.string.enter_how_long_is_your_period));
                }

                /*SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    Date date = sdf.parse(strDate);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    cal.add(Calendar.DATE, Integer.parseInt(strCycle));
                    Date date1 = cal.getTime();
                    String str1 = sdf.format(date1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }*/


            }
        });
        //endregion

        AlertDialog.Builder i_builder = new AlertDialog.Builder(getContext());

        mPeriodDialog = i_builder.create();
        mPeriodDialog.setCancelable(false);
        mPeriodDialog.setView(view);
        mPeriodDialog.show();

    }//endregion

    // region Period Calendar Dialog
    @SuppressLint("ResourceType")
    private void PeriodCalendarDialog(String startDate, String NextDate, String NoOfDays) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.row_dialog_period_calendar, null);

        CalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setHeaderColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        calendarView.setHeaderLabelColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        Calendar calendar1 = Calendar.getInstance();
        calendar1.add(Calendar.MONTH, 0);
        calendarView.setMinimumDate(calendar1);
        /*calendar1.add(Calendar.MONTH, 1);
        calendarView.setMaximumDate(calendar1);*/

        List<Calendar> calendars = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        int intNoOfDays = Integer.parseInt(NoOfDays);
        for (int i = 0; i < intNoOfDays; i++) {
            try {
                Calendar calendar = DateUtils.getCalendar();
                Date date = sdf.parse(startDate);
                calendar.setTime(date);
                calendar.add(DATE, i);
                calendars.add(calendar);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < intNoOfDays; i++) {
            try {
                Calendar calendar = DateUtils.getCalendar();
                Date date = sdf.parse(NextDate);
                calendar.setTime(date);
                calendar.add(DATE, i);
                calendars.add(calendar);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        calendarView.setSelectedDates(calendars);

        AlertDialog.Builder i_builder = new AlertDialog.Builder(getContext());

        mPeriodCalendarDialog = i_builder.create();
        mPeriodCalendarDialog.setCancelable(true);
        mPeriodCalendarDialog.setView(view);
        mPeriodCalendarDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mPeriodCalendarDialog.show();

    }//endregion

    // region DateDialog...
    @SuppressLint("ValidFragment")
    public static class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        Calendar myCalendar;
        TextView textView;

        public DateDialog(View v) {
            textView = (TextView) v;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceSateate) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            myCalendar = Calendar.getInstance();

            if (textView.getText().toString().equals("")) {
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            } else {
                String textDate = textView.getText().toString();
                try {
                    if (!textDate.equals("")) {
                        date = dateFormat.parse(textDate);
                        day = Integer.parseInt(dayFormat.format(date));
                        month = Integer.parseInt(monthFormat.format(date)) - 1;
                        year = Integer.parseInt(yearFormat.format(date));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            datePickerDialog.setTitle("");
            datePickerDialog.show();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, 1);
            Date date = calendar.getTime();
            long longDate = date.getTime();
            datePickerDialog.getDatePicker().setMaxDate(longDate);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);

            return datePickerDialog;

        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            textView.setText(populateSetDate(year, month + 1, day));

        }

        private String populateSetDate(int year, int i, int day) {

            String y = String.valueOf(year);
            String m = String.valueOf(i);
            String d = String.valueOf(day);

            String _Date = y + "-" + m + "-" + d;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat fmt2 = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat fmt3 = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date = fmt.parse(_Date);
                return fmt3.format(date);
            } catch (ParseException pe) {

                return "Date";
            }
        }

    }
    //endregion

    private String getNextDate(String strDate, String strCycle) {
        String str1 = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = sdf.parse(strDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(DATE, Integer.parseInt(strCycle));
            Date date1 = cal.getTime();
            str1 = sdf.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str1;
    }

    private String getEndDate(String strDate, String strCycle) {
        String str1 = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = sdf.parse(strDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(DATE, Integer.parseInt(strCycle) - 1);
            Date date1 = cal.getTime();
            str1 = sdf.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str1;
    }

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        gd.setColor(Color.parseColor("#FFFFFF"));
    }
    //endregion

    // region chanageEditTextBorder
    public void chanageTexViewtBorder(TextView textView) {
        textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        textView.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        gd.setColor(Color.parseColor("#FFFFFF"));
    }
    //endregion
}
