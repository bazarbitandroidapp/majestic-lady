package com.pureweblopment.majesticlady.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.pureweblopment.majesticlady.Adapter.PreviewViewPagerAdapter;
import com.pureweblopment.majesticlady.Adapter.ViewPagerAdapter;
import com.pureweblopment.majesticlady.Global.Global;
import com.pureweblopment.majesticlady.Global.SharedPreference;
import com.pureweblopment.majesticlady.Global.StaticUtility;
import com.pureweblopment.majesticlady.Global.Typefaces;
import com.pureweblopment.majesticlady.Model.Sliders;
import com.pureweblopment.majesticlady.R;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class PreviewActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    Context mContext = PreviewActivity.this;
    //widget
    private TextView mTxtSkip, mTxtDone;
    private CardView mCvDone;
    private ViewPager mVpPreview;
    private PageIndicatorView mPageIndicatorView;
    ArrayList<Sliders> sliders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        widgetInitialization();
        widgetTypefaces();
        widgetOnClickListener();
        widgetAPPSetting();
        getViewPager();
    }

    //region widgetAPPSetting
    private void widgetAPPSetting() {
        mTxtSkip.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        mTxtDone.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        mCvDone.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mPageIndicatorView.setSelectedColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mPageIndicatorView.setUnselectedColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
    }//endregion

    //region widgetInitialization
    private void widgetInitialization() {
        mTxtSkip = (TextView) findViewById(R.id.txtSkip);
        mTxtDone = (TextView) findViewById(R.id.txtDone);
        mVpPreview = (ViewPager) findViewById(R.id.vpPreview);
        mPageIndicatorView = (PageIndicatorView) findViewById(R.id.pageIndicatorView);
        mCvDone = (CardView) findViewById(R.id.cvDone);
    }//endregion

    //region widgetTypefaces
    private void widgetTypefaces() {
        mTxtSkip.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        mTxtDone.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
    }//endregion

    //region widgetOnClickListener
    @SuppressLint("NewApi")
    private void widgetOnClickListener() {
        mTxtSkip.setOnClickListener(this);
        mCvDone.setOnClickListener(this);
        mVpPreview.addOnPageChangeListener(this);
    }//endregion

    @Override
    public void onClick(View view) {
        Intent intentMainScreen = new Intent(mContext, MainActivity.class);
        switch (view.getId()) {
            case R.id.txtSkip:
                startActivity(intentMainScreen);
                finish();
                break;

            case R.id.cvDone:
                startActivity(intentMainScreen);
                finish();
                break;
        }
    }

    //region ViewPager..
    private void getViewPager() {
        sliders = new ArrayList<>();
        int i = 0;
        sliders.add(new Sliders(i++, "https://mahalaxmi-enterprises.s3.ap-south-1.amazonaws.com/images/2018/07/206864233e1f8ac90bd8a8a93667e1b9_1531820186_image_urlblog.jpg"));
        sliders.add(new Sliders(i++, "https://mahalaxmi-enterprises.s3.ap-south-1.amazonaws.com/images/2018/07/9a848f45fbe56cbc610c6a9bb83892d3_1532585412_image_urlblog.png"));
        sliders.add(new Sliders(i++, "https://mahalaxmi-enterprises.s3.ap-south-1.amazonaws.com/images/2018/07/1531815406_cosmetic-banner.png"));
        PreviewViewPagerAdapter viewPagerAdapter = new PreviewViewPagerAdapter(mContext, sliders);
        mVpPreview.setAdapter(viewPagerAdapter);

        /*final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == sliders.size()) {
                    currentPage = 0;
                }
                mVpPreview.setCurrentItem(currentPage++, true);
            }
        };

        Timer timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 5000);*/
    }
    //endregion

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < sliders.size(); i++) {
            if (position == sliders.size() - 1) {
                mCvDone.setVisibility(View.VISIBLE);
            } else {
                mCvDone.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
