package com.pureweblopment.majesticlady.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pureweblopment.majesticlady.Global.Typefaces;
import com.pureweblopment.majesticlady.Model.Testimonial;
import com.pureweblopment.majesticlady.R;

import java.util.ArrayList;

public class TestimonialAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private Fragment mFragment;
    private ArrayList<Testimonial> testimonials = new ArrayList<>();

    public TestimonialAdapter(Context mContext, Fragment mFragment, ArrayList<Testimonial> testimonials) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.testimonials = testimonials;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_testimonial, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            Testimonial testimonial = testimonials.get(i);
            viewHolder.mTxtTitle.setText(testimonial.getTitle());
            viewHolder.mTxtDes.setText(testimonial.getDes());
            viewHolder.mTxtCustomerName.setText(" - " + testimonial.getName());
        }
    }

    @Override
    public int getItemCount() {
        return testimonials.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtTitle, mTxtDes, mTxtCustomerName;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtTitle = itemView.findViewById(R.id.txtTitle);
            mTxtDes = itemView.findViewById(R.id.txtDes);
            mTxtCustomerName = itemView.findViewById(R.id.txtCustomerName);
            mTxtTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtCustomerName.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtDes.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        }
    }
}
