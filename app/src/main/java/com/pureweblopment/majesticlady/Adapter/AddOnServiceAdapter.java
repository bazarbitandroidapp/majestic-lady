package com.pureweblopment.majesticlady.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pureweblopment.majesticlady.Global.Typefaces;
import com.pureweblopment.majesticlady.Model.AddOnService;
import com.pureweblopment.majesticlady.R;

import java.util.ArrayList;

public class AddOnServiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Fragment mFragment;
    private ArrayList<AddOnService> addOnServices = new ArrayList<>();
    private boolean isShowMore;
    AddOnServiceEvent addOnserviceEvent;

    public AddOnServiceAdapter(Context mContext, Fragment mFragment,
                               ArrayList<AddOnService> addOnServices, boolean isShowMore) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.addOnServices = addOnServices;
        this.isShowMore = isShowMore;
    }

    public void setShowMore(boolean ShowMore, ArrayList<AddOnService> newAddOnServices) {
        isShowMore = ShowMore;
        /*for (int i = 0; i < newAddOnServices.size(); i++) {
            for (int j = 0; j < addOnServices.size(); j++) {
                if (newAddOnServices.get(i).getTitle().equalsIgnoreCase(addOnServices.get(j).getTitle())) {

                }
            }
        }*/
        addOnServices = newAddOnServices;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_add_on_services, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            if (!isShowMore) {
                if (position <= 2) {
                    viewHolder.mFlAddOnService.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.mFlAddOnService.setVisibility(View.GONE);
                }
            } else {
                viewHolder.mFlAddOnService.setVisibility(View.VISIBLE);
            }
            final AddOnService addOnService = addOnServices.get(position);
            viewHolder.mTxtTitle.setText(addOnService.getTitle());
            viewHolder.mTxtDes.setText(addOnService.getDes());
            viewHolder.mTxtPrice.setText(addOnService.getPrice());
            viewHolder.mFlMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addOnserviceEvent = (AddOnServiceEvent) mFragment;
                    if (!addOnService.isSelect()) {
                        addOnService.setSelect(true);
                        viewHolder.mCbAddOn.setChecked(true);
                        addOnserviceEvent.SelectedService(addOnService);
                    } else {
                        addOnService.setSelect(false);
                        viewHolder.mCbAddOn.setChecked(false);
                        addOnserviceEvent.SelectedService(addOnService);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return addOnServices.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox mCbAddOn;
        private TextView mTxtTitle, mTxtDes, mTxtPrice;
        private FrameLayout mFlMain, mFlAddOnService;

        public ViewHolder(View itemView) {
            super(itemView);
            mCbAddOn = itemView.findViewById(R.id.cbAddOn);
            mTxtTitle = itemView.findViewById(R.id.txtTitle);
            mTxtDes = itemView.findViewById(R.id.txtDes);
            mTxtPrice = itemView.findViewById(R.id.txtPrice);
            mFlMain = itemView.findViewById(R.id.flMain);
            mFlAddOnService = itemView.findViewById(R.id.flAddOnService);

            mTxtTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtDes.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtPrice.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        }
    }

    public interface AddOnServiceEvent {
        void SelectedService(AddOnService addOnService);
    }
}
