package com.pureweblopment.majesticlady.Model;

public class Testimonial {
    String Title;
    String Des;
    String Name;

    public Testimonial(String title, String des, String name) {
        Title = title;
        Des = des;
        Name = name;
    }

    public String getTitle() {
        return Title;
    }

    public String getDes() {
        return Des;
    }

    public String getName() {
        return Name;
    }
}
