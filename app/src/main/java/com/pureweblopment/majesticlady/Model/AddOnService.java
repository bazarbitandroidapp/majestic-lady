package com.pureweblopment.majesticlady.Model;

public class AddOnService {
    String Title;
    String Des;
    String Price;
    boolean isSelect;

    public AddOnService(String title, String des, String price) {
        Title = title;
        Des = des;
        Price = price;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String des) {
        Des = des;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
